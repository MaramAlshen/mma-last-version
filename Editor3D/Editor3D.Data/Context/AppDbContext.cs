﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Editor3D.Entities;
using Editor3D.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace Editor3D.Data
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {
        }

       // public DbSet<Vendor> Vendors { get; set; }
        //public DbSet<Anatomic> Anatomics { get; set; }
        public DbSet<GNote> GNotes { get; set; }
        public DbSet<Scene> Scenes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            //Flount API        
            modelBuilder.Entity<Scene>(entity =>
            {
                entity.HasKey(e => e.SceneId);
            });

            modelBuilder.Entity<GNote>(entity =>
            {
                entity.HasKey(e => e.GNoteId);
            });
        }
    }
}