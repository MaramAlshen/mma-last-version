import { Injectable } from '@angular/core';
// import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { post } from 'selenium-webdriver/http';

const httpOptions = {
  headers: new Headers({ 'Content-Type': 'application/json' })
};

@Injectable()

export class MainService {
    public vendorName = new BehaviorSubject<string>('');
    currentVendor = this.vendorName.asObservable();

    private objName = new BehaviorSubject<Object>({ 'anatomicName' : "1", 'vendorName' : "2", 'Action' : "3", 'time' : "4"});
    currentObj = this.objName.asObservable()

    private arrName = new BehaviorSubject<Array<Object>>([]);
    currentArr = this.arrName.asObservable()

    constructor(private http: Http, private httpClient: HttpClient) { }

    addVendor(vendorFileName: string) {
        this.vendorName.next(vendorFileName)
    }

    generateNote(Anatomic: string, Vendor:string,Action:string) {
        let obj:Object={
            "Action": Action,
            "Vendor": Vendor,
            "Anatomic": Anatomic,
            time: new Date()
        }

        this.objName.next(obj);
    }

    sendArray(arr: Object[]) {
        this.arrName.next(arr);
        let c = this.getNotes();
        this.postAllNotes(this.arrName.value[0])
    }

    getNotes() {
        this.http.get('http://localhost:63728/api/gnote')
            .map((data: Response) => {
                return data.json() as Object[];
            }).toPromise().then(x => { })
    }

    postAllNotes(data: {}) {       
        let body = JSON.stringify(data);
        let headerOptions = new Headers({ 'Content-Type': 'application/json' });
        let requestOptions = new RequestOptions({ method: RequestMethod.Post, headers: headerOptions });
        return this.http.post('http://localhost:63728/api/gnote', body, requestOptions).map(x => x);
    }

}
