import { Component, Inject, OnInit } from '@angular/core';
import { MainService } from "../../main.service";
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Response, RequestOptions, Headers, RequestMethod } from '@angular/http';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { post } from 'selenium-webdriver/http';

const httpOptions = {
    headers: new Headers({ 'Content-Type': 'multipart/form-data' })
};

@Component({
    selector: 'fetchdata',
    templateUrl: './fetchdata.component.html',
    styleUrls: ['./fetchdata.component.css']
})

export class FetchDataComponent implements OnInit {

    private allNotes: Object[] = [];
    public notes : any;

    constructor(private _demoService: MainService, private data: MainService, private http: Http, private httpClient: HttpClient) { }

    ngOnInit() {
        this.data.currentObj.subscribe(
            (object : any) => {
                this.allNotes.push(object)
            }
        )
    }

    public noteInfo(object : any): void { }

    public postAllNotes(): void {
        for(let i=1;i<this.allNotes.length;i++){
            this._demoService.postAllNotes(this.allNotes[i]).subscribe(
                // the first argument is a function which runs on success
                (data: any) => {
                    this.notes = data;
                },
                // the second argument is a function which runs on error
                (err : any) => console.error(err),
                // the third argument is a function which runs on completion
                () => console.log('done loading notes')
            );
        }
    }
}



