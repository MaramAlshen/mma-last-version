//import { Component } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Component, OnInit } from '@angular/core';
import { MainService } from "../../main.service";
import * as BABYLON from 'babylonjs';

@Component({
    selector: 'counter',
    templateUrl: './counter.component.html',
    styleUrls: ['./counter.component.css']
})

export class CounterComponent implements OnInit {

    private canvas: any;
    private engine: BABYLON.Engine;
    private camera: BABYLON.ArcRotateCamera;
    private scene: BABYLON.Scene;
    private light: BABYLON.Light;
    private light2: BABYLON.PointLight;
    private hl2: BABYLON.HighlightLayer;
    private vendor: string = "";
    private selected: any;
    public selectedArray: Array<any> = [];

    constructor(private data: MainService) { }


    ngOnInit() {
        this.data.currentVendor.subscribe(
           (vendor) => {
               // this.canvas?this.import(vendor):false;
               // && !this.scene.meshes[4]
               if (this.canvas ) {
                   this.import(vendor)
               } else { false }
           }
        )
        this.canvas = document.getElementById('renderCanvas');
        this.engine = new BABYLON.Engine(this.canvas, true);
        this.createScene();
        this.animate();
        //this.highLight();
        this.click();
        this.rightClick();
    }

    public createScene(): void {
        this.scene = new BABYLON.Scene(this.engine);
        this.camera = new BABYLON.ArcRotateCamera("Camera", -21, 1.5, 20, BABYLON.Vector3.Zero(), this.scene);
        this.canvas.style.backgroundColor = "white";
        this.camera.attachControl(this.canvas, true);
        this.light = new BABYLON.HemisphericLight('light1', new BABYLON.Vector3(0, 1, 0), this.scene);
        this.light2 = new BABYLON.PointLight('l2', new BABYLON.Vector3(0, 1, -1), this.scene);
        this.light2.position = this.camera.position;
        this.hl2 = new BABYLON.HighlightLayer("hl2", this.scene);
        //BABYLON.SceneLoader.ImportMesh("", "https://bitbucket.org/MaramAlshen/assets/raw/master/", "Central_Incisor_11n.babylon", this.scene, (result) => {
           //result[0].scaling.x = 10;
           //result[0].scaling.y = 10;
           //result[0].scaling.z = 10;
           //result[0].position.z = 1.5;
            //console.log(",,,,,,,,,,,,,,,,,,,,",this.scene.meshes)
        //})

        //let s = BABYLON.Mesh.CreateBox("sphere1", 2, this.scene);
        //var groundMat = new BABYLON.StandardMaterial("groundMat", this.scene);
        //groundMat.diffuseColor = new BABYLON.Color3(0.5, 0.8, 1);
        //s.material = groundMat;
        //s.visibility = 0.2
        //s.rotation.y = Math.PI / 3;

    }

    public rightClick(): void {
        this.canvas.addEventListener('contextmenu', (event: any) => {
            let pickResult: any = this.scene.pick(this.scene.pointerX, this.scene.pointerY);
                // && pickResult.pickedMesh.name !== 'Central_incisor_11'
            if (pickResult.hit) {
                this.selected = pickResult.pickedMesh;
                // console.log('aaa  ', this.selected);

                let context: any = document.getElementById('context');
                context.style.zIndex = 9999999;
                context.style.position = 'absolute';
                context.style.left = event.clientX + 10 + 'px';
                context.style.top = event.clientY + 10 + 'px';
                context.style.display = 'block';
                document.getElementById('context2')!.style.display = 'none';
            } else {
                let context: any = document.getElementById('context2');
                context.style.zIndex = 9999999;
                context.style.position = 'absolute';
                context.style.left = event.clientX + 10 + 'px';
                context.style.top = event.clientY + 10 + 'px';
                context.style.display = 'block';
                document.getElementById('context')!.style.display = 'none';
            }
            event.preventDefault();
        }, false);

        this.canvas.addEventListener('click', (event: any) => {
            let context: any = document.getElementById('context');
            let context2: any = document.getElementById('context2');
            context.style.display = 'none';
            context2.style.display = 'none';
            event.preventDefault();
        }, false);
    }

    public replaceFunc(): void {       
        let note = prompt('replace');
        if (note !== null)
            this.addNote(note, 'Note', 'Replace');
    }

    public editFunc(): void {
        let note = prompt('edit');
        if (note !== null)
            this.addNote(note, 'Note', 'Edit');
    }

    public import(vendorFileName: string): void {
        BABYLON.SceneLoader.ImportMesh("", "https://bitbucket.org/MaramAlshen/assets/raw/master/", vendorFileName, this.scene, (result) => {
            //result[0].scaling.x = 5;
            //result[0].scaling.y = 5;
            //result[0].scaling.z = 5;
            //result[0].position.x = 7;
            //for (let i = 0; i < this.scene.meshes.length; i++) {
            //    console.log("...............", i + ": ", this.scene.meshes[i].name, this.scene.meshes[i]);
            //}
            this.addNote(result[0].name, '........', 'Add');
        })
    }

    public animate(): void {
       this.engine.runRenderLoop(() => {
           this.scene.render();
       });

       window.addEventListener('resize', () => {
           this.engine.resize();
       });
    }

    public highLight(): void {
        let lastMesh : any = null;
        this.canvas.addEventListener('mousemove', () => {
            let pickResult = this.scene.pick(this.scene.pointerX, this.scene.pointerY);
            if (pickResult!.hit && !lastMesh) {
                let hl = new BABYLON.HighlightLayer("hl1", this.scene);
                hl.addMesh(<any>pickResult!.pickedMesh, BABYLON.Color3.Green());
                lastMesh = pickResult!.pickedMesh!.name;
            }
            if (!pickResult!.hit || pickResult!.pickedMesh!.name !== lastMesh) {
                this.scene.effectLayers = [];
                lastMesh = null;
            }
        })
    }

    public click(): void {
        this.canvas.addEventListener('click', () => {
            let pickResult = this.scene.pick(this.scene.pointerX, this.scene.pointerY);
            if (pickResult && pickResult.hit) {
                this.selectedArray.push(pickResult.pickedMesh);
                console.log("...............", this.selectedArray);
                this.hl2.addMesh(<any>pickResult!.pickedMesh, BABYLON.Color3.White());
            }  
        })
    }

    public removeAll(): void {
        if (window.confirm('Are you sure ?')) {
            if (this.selectedArray.length) {
                for (let i = 0; i < this.selectedArray.length; i++) {
                    this.remove(this.selectedArray[i]);
                }
                this.selectedArray = [];
            }
        }
    }

    public remove(element : any): void {
        // determine if Vendor Or Anatomin
        if (element.parent && element.parent.parent && element.parent.parent.name === "Teeth") {
            // if Amatomy -> remove only the tooth
            // generate a Note
            this.addNote(element.name, '.......', 'Remove');
            element.dispose();
        } else {
            // if Vendor -> remove the hole Vendor
            // generate a Note
            if (element.parent !== undefined) {
                if (element.parent.parent !== undefined) {
                    this.addNote(element.parent.parent.name, '.......', 'Remove');
                    element.parent.parent.dispose();
                } else {
                    this.addNote(element.parent.name, '.......', 'Remove');
                    element.parent.dispose();
                } 
                //this.selected.parent.dispose();
            } else {
                this.addNote(element.name, '.......', 'Remove');
                element.dispose();
            } 
        }

    }

    public addNote(Action: string, Vendor: string, Anatomic: string): void {
       this.data.generateNote(Action, Vendor, Anatomic);
    }

}
