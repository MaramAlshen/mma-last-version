import { Component, OnInit } from '@angular/core';
import { MainService } from "../../main.service";
import { BehaviorSubject } from 'rxjs/BehaviorSubject'
@Component({
    selector: 'nav-menu',
    templateUrl: './navmenu.component.html',
    styleUrls: ['./navmenu.component.css']
})


export class NavMenuComponent implements OnInit {

    public vendorName: string | undefined;

    constructor(private data: MainService) { }

    public ngOnInit() : void {
        this.data.currentVendor.subscribe(vendor => this.vendorName = vendor)
    }

    addVendor(): void {
        let vendorFileName:any = (<HTMLInputElement>document.getElementById('selectOption')).value;
        this.data.addVendor(vendorFileName)
    }
}
