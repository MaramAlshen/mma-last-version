﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Editor3D.Data;
using Editor3D.Data.Interfaces;
using Editor3D.Entities.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Editor3D.WebUI.Controllers
{
    [EnableCors("AllowAnyOrigin")]
    [Produces("application/json")]
    [Route("api/GNote")]
    public class GNoteController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IGNote _gNote;

        //Constracer
        public GNoteController(IGNote gNote,AppDbContext appDbContext)
        {
            _context = appDbContext;
            _gNote = gNote;
        }
        //............................................
        // GET: api/GNote .............get all GNotes
        [HttpGet]
        public  IEnumerable<GNote> GetGNote()
        {
            return  _gNote.GetAllGNotes();
        }
        //............................................
        //GET: api/GNote/5 ..........get GNote by Id
        [HttpGet("{id}")]
        public async Task<IActionResult> GetGNote([FromRoute] int Id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            var gNote = _gNote.GetGNoteById(Id);
            if (gNote == null)
            {
                return NotFound();
            }
            return Ok(gNote);
        }
        //.............................................
        //Post: api/GNote
        [HttpPost]
        public IActionResult  PostGNote([FromBody] GNote gNote)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _gNote.GeneratNote(gNote);

            return Ok("done..............esraa");
        }
    }
}
