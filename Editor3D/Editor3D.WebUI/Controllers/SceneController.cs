﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Editor3D.Data;
using Editor3D.Data.Interfaces;
using Editor3D.Entities.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Editor3D.WebUI.Controllers
{
    [EnableCors("AllowAnyOrigin")]
    [Produces("application/json")]
    [Route("api/Scene")]
    public class SceneController : Controller
    {
        private readonly AppDbContext _context;
        private readonly IScene _scene;

        //Constracter
        public SceneController(IScene scene, AppDbContext appDbContext)
        {
            _context = appDbContext;
            _scene = scene;
        }
        //.............................................
        //GET: api/Scene.................get last Scene
        [HttpGet]
        public IEnumerable<Scene> GetScene()
        {
            return _scene.GetLastScene();
        }
        //.............................................
        //Post: api/Scene
        [HttpPost]
        public IActionResult PostScene([FromBody] Scene scene)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _scene.AddScene(scene);

            return Ok("done..............esraa");
        }
    }
}
